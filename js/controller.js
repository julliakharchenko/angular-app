var app = angular.module('myApp', ['ngRoute', 'ui.bootstrap']);

/*--- router configuratoins ---*/
app.config(function($routeProvider, $httpProvider){

  $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

  $routeProvider
  .when('/', {
    templateUrl: 'templates/login.html'
  })
  .when('/dataGrid', {
    resolve: {
      "check": function($location, $rootScope) {
        if(!$rootScope.loggedIn){
          $location.path('/');
        }
      }
    },
    templateUrl: 'templates/dataGrid.html'
  })
  .otherwise({
    redirectTo: '/'
  });
});

/*--- login controller configuratoins ---*/
app.controller('loginController', function($scope, $location, $rootScope,$http) {
  $scope.errorLogin = false;

  /*--- on click submit button function ---*/
  $scope.submit = function(){


  var req = {
   //method: 'JSONP',
   method: 'POST',
   //url: 'http://165.227.160.52:8080/login',
   url: 'data/login-data.json',
   //url: 'http://s1.devcodin.com/learnJS/login-data',
   headers: {
     'Accept': 'Accept:application/json, Content-Type:application/json'
   },
   data: { "login":$scope.uname, "password":$scope.pass }
  };
  /*--- check values fron login form from server ---*/
   $http(req)
   .then(function success(response){
     if($scope.uname == response.data.login && $scope.pass == response.data.password) {
       $rootScope.loggedIn = true;
       $scope.errorLogin = false;
       $location.path('/dataGrid');
     } else {
       $scope.errorLogin = true;
     //  console.log($scope.errorLogin);
     }
   },
   function errorCallback(response){
     console.log('error');
   });
  };

  /*--- function show error box ---*/
  $scope.checkForm = function(){
    $scope.errorLogin = false;
  };
});

/*--- data table controller configuratoins ---*/
app.controller('dataTable', function($scope, $http, $modal){

  
  /*--- get data from server ---*/
  $http({
    method: 'GET',
    url: 'data/countries.json',
    //url: 'http://s1.devcodin.com/learnJS/countries',
    headers: 'Accept:application/json, Content-Type:application/json'
  })
  .then(function successCallback(response) {
    
    $scope.countries = response.data;
   
    
  //  console.log($scope.countries);
  },
  function errorCallback(response){
    console.log('error');
  });
  
 /* $scope.dialogWindow = function () {
      $scope.listData = dataTable;
        $scope.checkItem = "yes";
        $modal.open({
            templateUrl: 'modal.html',
            controller: 'modalController'
        })
        .result.then(function() {
            alert('closed');
        }, function() {
            alert('canceled');
        });
    };*/
});
/* app.controller('modalController', ['$scope', function($scope) {
    
}]); */